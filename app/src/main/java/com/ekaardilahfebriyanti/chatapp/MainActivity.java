package com.ekaardilahfebriyanti.chatapp;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {
    private WebSocketClient mWebSocketClient;

    private TextView mEditTextChats;
    private EditText mEditTextMessage;
    private Button mButtonSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditTextChats = findViewById(R.id.tvTextChats);
        mEditTextMessage = findViewById(R.id.editTextMessage);
        mButtonSend = findViewById(R.id.buttonSend);

        connectWebSocket();

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });
    }
    private void connectWebSocket(){
        URI uri;
        try {
            uri = new URI("ws://172.22.62.4:8887"); //alamat ip komputer
//            uri = new URI("ws://172.22.61.31:8887"); //alamat ip komputer

        }catch (URISyntaxException e){
            e.printStackTrace();
            return;
        }
        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                Log.i("websocket", "Opened");
                mWebSocketClient.send("Hello from"+ Build.MANUFACTURER+" "+Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mEditTextChats.append(message+"\n");
                    }
                });
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                Log.i("websocket", "Closed "+reason);
            }

            @Override
            public void onError(Exception ex) {
                Log.i("websocket", "Error "+ ex.getMessage());
            }
        };
        mWebSocketClient.connect();
    }
    private void attemptSend(){
        String message = mEditTextMessage.getText().toString().trim();
        if(TextUtils.isEmpty(message)){
            return;
        }
        mEditTextMessage.setText("");
        mWebSocketClient.send("eka : "+message);
    }
}
